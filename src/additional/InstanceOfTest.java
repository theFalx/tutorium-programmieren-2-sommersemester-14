package additional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author falx
 * @version v0.1a
 */
public class InstanceOfTest
{
    public static void main( String[] args )
    {
        String s = "test";
        List<Integer> l = new ArrayList<Integer>(  );
        System.out.println( s.getClass().equals( Integer.class ) );
        System.out.println( Integer.class.isInstance( s ) ); // isInstance ist mittels JNI implementiert!
        System.out.println( String.class.isInstance( s ) );
        System.out.println( ArrayList.class.isInstance( l ) );
        System.out.println( l instanceof Collection );
        System.out.println( Collection.class.isInstance( l ) ); // schaut ob die Klassen im selben Vererbungsast hängen
        System.out.println( Collection.class.equals( l.getClass() ) ); // vergleicht die Klassentypen
    }
}
