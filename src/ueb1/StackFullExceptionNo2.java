package ueb1;

/**
 * @author falx
 * @version v0.1a
 */
public class StackFullExceptionNo2 extends Exception
{
    public StackFullExceptionNo2( String msg )
    {
        super( msg );
    }

    public StackFullExceptionNo2()
    {
        this( "Stack is already full!" );
    }
}
