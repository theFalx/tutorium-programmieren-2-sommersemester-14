package ueb1;

/**
 * @author falx
 * @version v0.1a
 */
public class Queue extends AbstractQueue
{
    public Queue( int size )
    {
        super( size );
    }

    @Override
    public boolean enqueue( Object o )
    {
        if( tailptr < queue.length && fill != queue.length )
        {
            queue[tailptr++] = o;
            tailptr %= queue.length;
            if( headptr == -1 )
                headptr = 0;
            fill++;
            return true;
        }
        else
            return false;
    }

    @Override
    public int getFreeSpace()
    {
        return ( headptr > tailptr ) ?
                    headptr - tailptr :
                    queue.length - tailptr + ( ( headptr == -1 ) ?
                                                0 :
                                                headptr );
    }
}
