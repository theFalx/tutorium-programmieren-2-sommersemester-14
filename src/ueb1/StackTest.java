package ueb1;

/**
 * @author falx
 * @version v0.1a
 */
public class StackTest
{
    public static void main( String[] args ) throws StackFullException
    {
        //IStack stack = new Stack(10);  // Diesen Stack nutzen um das Muster zu testen
        IStack stack = new StackNo2(10); // Diesen Stack nutzen um den im Tutorium erarbeiteten zu nutzen
        Integer[] arr = new Integer[11];

        for( int i = 0; i < arr.length; i++ )
            arr[i] = i;

        try {
            System.out.println( "---StackFullException-Test started---" );
            for( int i = 0; i < arr.length; i ++ )
            {
                stack.push( arr[i] );
                System.out.printf( "\tpushed %d\n", arr[i] );
            }
        }
        catch( StackFullException exc ) {
            System.out.println( "\tstackfull thrown!" );
        }

        System.out.println( "---StackFullException-Test finished---\n\n" );

        System.out.println( "---Pop-Test started---" );
        Integer integer;
        do
        {
            integer = (Integer)stack.pop();
            System.out.printf( "poped %d\n", integer );
        }while( integer != null );
        System.out.println( "---Pop-Test finished---\n\n" );

        System.out.println( "---Another-Test started---" );
        int k = 0;
        int j;
        for(  ; k < (arr.length - 1); k++ )
        {
            stack.push( arr[k] );
            System.out.printf( "pushed %d\n", arr[k] );
        }
        k--;

        System.out.println(  );

        j = k;
        for( ; k > (j - 3); k-- )
        {
            integer = (Integer)stack.pop();
            System.out.printf( "poped %d\n" , arr[k] );
        }
        k++;

        System.out.println( "\n" );

        for( int i = 0; i <= ((j-k) + 1); i++, k++ )
        {
            stack.push( arr[k] );
            System.out.printf( "pushed %d\n", arr[k] );
        }

        System.out.println(  );

        do
        {
            integer = (Integer)stack.pop();
            System.out.printf( "poped %d\n", integer );
        }while( integer != null );
        System.out.println( "---Another-Test finished---" );
    }
}
