package ueb2;

import java.util.ArrayList;

/**
 * @author falx
 * @version v0.1a
 */
public class Stack<T>
{
    private ArrayList<T> mStack;
    private int mSize;
    private int mPointer;

    /**
     *
     * @param size initial size for the Stack
     */
    public Stack( int size )
    {
        if( size <= 0 )
            throw new IllegalArgumentException( "Size must be greater 0!" );

        this.mSize    = size;
        this.mStack   = new ArrayList<>( size );
        this.mPointer = 0;
    }


    /**
     *
     * @param element the element to push
     * @return true if push was successful else false
     */
    public boolean push( T element )
    {
        if( mPointer >= mSize )
            return false;

        mStack.add( mPointer++, element );
        return true;
    }


    /**
     *
     * @return the object on top of the stack or null if empty
     */
    public T pop()
    {
        if( mPointer <= 0 )
            return null;

        return mStack.remove( --mPointer );
    }


    /**
     *
     * @return the size of the stack
     */
    public int getSize()
    {
        return mSize;
    }


    /**
     *
     * @param size new size of the stack, must be greater than the current size
     */
    public void setSize( int size )
    {
        if( size <= mSize )
            throw new IllegalArgumentException( "size must be greater the current size!" );

        mSize = size;
    }
}
