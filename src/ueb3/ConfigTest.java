package ueb3;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Map;

/**
 * @author falx
 * @version v0.1a
 */
public class ConfigTest
{
    public static void main( String[] args ) throws FileNotFoundException
    {
        URL path = ConfigTest.class.getResource( "config.cfg" );
        System.out.println( path );
        ConfigurationManager configManager = new ConfigurationManager( new File( path.getFile() ) );

        System.out.println( configManager.readFile() );
        Map<String, String> map = configManager.getConfigurationMap();

        for( String s : map.keySet() )
            System.out.printf( "Key = %s \t|\tValue = %s\n", s, map.get( s ) );
    }
}
